'use strict'

/**
 * Dependencies
 */
const _ 	 = require('lodash');
const debugF  = require('debug')('router:forwarding');
const debugC  = require('debug')('router:app');
const _conf  = require('./conf');
const _util  = require('./util')
const Client = require('./mySocket')
const Node 	 = require('./Node')
const DV 	 = require('./DV')
const purdy  = require('purdy');
const mySocket = require('./mySocket')
const Message = require('./Message')

/**
 * Server Actions
 */
const actions = {
	onEnd: () => {
		debugF('> client disconnected')
	},
	onData: (server, data) => {
		let res = _util.parseInfo2(data.toString());

		debugF(`> from: ${res.from} to: ${res.to}`)
		debugF(`> raw data: -->${data.toString()}<--`)

		// es para mi el mensaje
		if(_conf.name==res.to){
			res.status = 'recieved'
			Message.create(res);
		} else {
			actions.sendMessage(res, function(answer){
				res.status = answer;
				Message.create(res);
				console.log(':: ', answer)
			});
		}
	},
	// FORWARDING
	sendMessage: (res, cb)=> {
		if(res.from==undefined)
			res.from = _conf.name;

		// revisar si esta en la tabla
		if(_SELF.dv[res.to]){
			let via = _SELF.dv[res.to].via;
			via = (via=='!')? 'F':via;
			
			if(via=='!' || _nodes[via]==undefined || !_nodes[via].status){
				cb('fail')
			} else {
				let client = require('net').connect({host: _nodes[via].ip, port: 1981}, ()=>{
					client.write(`FROM:${res.from}\nTO:${res.to}\nMSG:${res.msg}\nEOF\n\r\r`);
					client.end();
					cb('success');
				});
				client.on('error', e => {
					cb('fail');
					console.error('app> ', e)
				})
			}
		} else {
			cb('fail');
		}
	},

	onError: e => {
		debugF(e)
	}
}


// BIND
module.exports = {
	forwarding: server => {
		debugF('> client connected')
		server.on('end', actions.onEnd)
		server.on('data', data => actions.onData(server, data))
		server.on('error', e => actions.onError(e))
	},
	application: server=> {
		debugC('> client connected')
		server.on('end', ()=>{
			debugC('> client disconnected')
		})
		server.on('data', data => {
			debugC('> recieved data')
			let res = JSON.parse(data.toString())

			actions.sendMessage(res, function(data) {
				server.write(data)
			})
		})
		
		server.on('error', e => {
			debugC('err>', e)
		})	
	}
}






