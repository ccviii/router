'use strict';

/**
 * Dependencies
 */
const _ = require('lodash')
const net 	= require('net');
const debug = require('debug');
const _conf = require('./conf');
const _util = require('./util');
const KeepAliveTimeout = _conf.KeepAliveTimeout*1000;
const Node = require('./Node')

/**
 * Client Defualt Actions
 */
const actions = {
	onEnd: (debug, id) => {
		_nodes[id].status = false;
		Node.update(_nodes[id]);
		// _sendDV();
		debug(`end> [${id}] disconnected from server`);
		_util.resetHandshake(id);
	},
	onData: (debug, client, data, id) => {
		let res = _util.parseInfo(data.toString());
		debug(`reci> [${id}] from: ${res.name} type: ${res.type}`);
  		// client.end();
	},
	onStart: (debug, client, id, uuid) => {
		
	},
	onError: (debug, e, id, client) => {
		_nodes[id].status = false;
		// _sendDV();
		Node.update(_nodes[id]);
		debug(`error> [${id}] <${e.address}> not responded`)
		_util.resetHandshake(id);
		client.end();
	},
	onTimeout: (debug, client, id) => {
		if(!_sendDV()) {
			debug(`send> keep alive to [${id}]`)
			client.write(`FROM:${_conf.name}\nType:KeepAlive\n\r\r`)
		} else {
			debug(`> update DV`)
		}
	},
	onClose: () => {

	}
}

/**
 * Binding, create mySocket
 */
function mySocket(options, id, node){
	this.debug = debug('router:'+node.name);
	this.id = id;
	this.node = node;

	this.sendHello = () => {
		_nodes[node.name].hello.sent = true;
		_nodes[node.name].status = true;
		Node.update(_nodes[node.name]);
		this.debug(`send> [${node.name}] send hello to ${node.name}`)
  		this.client.write(`From:${_conf.name}\nType:HELLO\n\r\r`);
	}
	
	this.client = net.connect(options, this.sendHello);
	this.client.setTimeout(KeepAliveTimeout);
	this.client.on('data', data => actions.onData(this.debug, this.client, data, this.node.name));
	this.client.on('end', () => actions.onEnd(this.debug, this.node.name));
	this.client.on('error', e => actions.onError(this.debug, e, this.node.name, this.client));
	this.client.on('timeout', () => actions.onTimeout(this.debug, this.client, this.node.name))

	this.send = data => {
		console.log('sent to: ', this.node.name)
		console.log(data)
		this.client.write(data+'\n\r\r')
	}

	this.sendDV = () => {
	}

	

	this.reconnect = () => {
		console.log("reconnecting!")
		this.client = net.connect(options, ()=>{
			this.sendHello();
			this.send(`FROM:${_conf.name}\nTYPE:WELCOME`)
			// _sendDV();
		});
		this.client.setTimeout(KeepAliveTimeout);
		this.client.on('data', data => actions.onData(this.debug, this.client, data, this.node.name));
		this.client.on('end', () => actions.onEnd(this.debug, this.node.name));
		this.client.on('error', e => actions.onError(this.debug, e, this.node.name, this.client));
		this.client.on('timeout', () => actions.onTimeout(this.debug, this.client, this.node.name))
	}
}

module.exports = mySocket;




