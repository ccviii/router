'use strict'

// depend
const _ 	= require('lodash');
const _config = require('./conf');
let warning = true;


/**
 * Utilities
 */
module.exports = {
	parseInfo: message => {
		let name = message.substr(message.indexOf(':')+1, message.indexOf('\n') - message.indexOf(':')-1)
		message = message.toLowerCase();
		
		let type = message.substr(message.indexOf('type:')+('type:').length)
		name = name.trim().toUpperCase();
		type = type.trim().toLowerCase();

		let dv = []
		
		if(type.indexOf('dv')!=-1){
			dv = type.substr(2).split('\n')
			let aux = {}
			for (var i = 0; i < dv.length; i++) {
				if(dv[i].indexOf(':')==1)
					aux[dv[i].substr(0, 1)] = parseInt(dv[i].substr(2, 3));
			};
			dv = aux;
			type = type.substr(0, 2)
		}

		

		return {
			name: name,
			type: type,
			dv: dv
		}
	},

	parseInfo2: message => {
		// FROM:G
		// TO:F
		// MSG:si que tal
		// EOF
		let from = message.substr(message.indexOf(':')+1, message.indexOf('\n') - message.indexOf(':')-1)
		message = message.substr(message.indexOf('\n')+1)
		let to = message.substr(message.indexOf(':')+1, message.indexOf('\n') - message.indexOf(':')-1)

		message = message.substr(message.indexOf('\n')+1)
		let msg = message.substr(message.indexOf(':')+1, message.indexOf('EOF') - message.indexOf(':')-1)

		return {
			from: from.toUpperCase(),
			to: to.toUpperCase(),
			msg: msg.trim()
		}
	},

	array2Object: nodes => {
		return _.reduce(nodes, (acum, node) => {
			// delete node.client
			acum[node.name] = node;
			return acum;
		}, {})
	},

	resetHandshake: id => {
		_nodes[id].hello['sent'] = false;
		_nodes[id].hello['recieved'] = false;
		_nodes[id].welcome['sent'] = false;
		_nodes[id].welcome['recieved'] = false;
	},

	log: (data, flag) => {
		if(!_config.pretty){
			// nothing
		} else if(require('purdy')){
			const purdy = require('purdy')
			if(flag && typeof data == "object") {
				for (var i = 0; i < Object.keys(data).length; i++) {
					var obj = data[Object.keys(data)[i]]
					var toprint = {};
					for (var j = 0; j < Object.keys(obj).length; j++) {
						if(Object.keys(obj)[j]!="client")
							toprint[Object.keys(obj)[j]] = obj[Object.keys(obj)[j]]
					};
					purdy(toprint)
				};
			} else {
				purdy(data);
			}
		} else {
			if(warning){
				console.log("> should install purdy; npm i --save-dev purdy ;)")
				warning = false;
			}
			console.log("> ", data)
		}
	}
}