/**
 * Messages Model
 */

module.exports = (() => {
	// Dependencies
	const r 		= require('rethinkdb')
	const debug 	= require('debug')
	const Q 		= require('q')

	// Private variables
	const _options = {}
	const _db = 'router'
	const _table = 'dv'

	// Public Methods
	const model = {
		save: obj => {
			model.find().then(function(datos){
				if(datos.length==0){
					model.create(obj)
				} else {
					obj.id = datos[0].id;
					model.update(obj)
				}
			})
		},
		create: obj => {
			var defer = Q.defer()

			r.connect(_options).then(conn => {
				// obj.createdAt = r.now()
				// obj.updatedAt = r.now()

				return r.db(_db).table(_table).insert(obj).run(conn)
					.finally(() => conn.close())
			})
			.then ( output  => defer.resolve(output) )
			.error(	err 	=> defer.reject (err) 	 )

			return defer.promise
		},

		find: function(key){
			var defer = Q.defer()

			r.connect(_options).then(conn => {
				if(typeof key == "string") {
					return r.db(_db).table(_table).get(key).run(conn)
						.finally(() => conn.close() )
				} else {
					return r.db(_db).table(_table).run(conn)
						.finally(() => conn.close() )
				}
			
			})
			.then( cursor => cursor.toArray() 	)
			.then( output => defer.resolve(output))
			.error(   err => defer.reject(err) 	)
			
			return defer.promise
		},
		update: function(object){
			
			var defer = Q.defer()

			r.connect(_options).then(conn => {
				// object.updatedAt = r.now()

				return r.db(_db).table(_table).get(object.id).update(object).run(conn)
					.finally(() => conn.close() )
			}).then( output => defer.resolve(output)
			).error(   err => defer.reject(err) 	)

			return defer.promise
		},

		delete: function(id){
			var defer = Q.defer()

			r.connect(_options).then(function(conn){
				return r.db(_db).table(_table).get(id).delete().run(conn)
					.finally(() => conn.close() )
			}).then( output => defer.resolve(output))
			.error(   err => defer.reject(err) 	)

			return defer.promise
		},

		deleteAll: function(){
			var defer = Q.defer()

			r.connect(_options).then(function(conn){
				return r.db(_db).table(_table).delete().run(conn)
					.finally(() => conn.close() )
			}).then( output => defer.resolve(output))
			.error(   err => defer.reject(err) 	)

			return defer.promise
		}
	}

	return model
}())