/**
 *	Server
 */

/**
 * Dependencies
 */
const net 	= require('net')
const serverActions = require('./serverActions')
const actions = require('./forwardingActions')
const _conf = require('./conf')

/**
 * Main
 */

const server = net.createServer(serverActions);
server.listen(_conf.port, () => require('debug')('router:server')(`> started, listening on port ${_conf.port}`))


const forwarding = net.createServer(actions.forwarding);
forwarding.listen(1981, () => require('debug')('router:forwarding')(`> started, listening on port ${1981}`))

const application = net.createServer(actions.application);
application.listen(9081, () => require('debug')('router:application')(`> started, listening on port ${9081}`))
