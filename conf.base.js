'use strict';

module.exports = {
	name: 'J',
	port: '9080',
	nodes: [
		{
			name	: 'G',
			cost	: 3,
			ip		: '192.168.43.119',
			port	: 9080,
			status	: false,
			hello	: {
				sent: false,
				recieved: false
			},
			welcome	: {
				sent: false,
				recieved: false
			}
		},
		{
			name	: 'K',
			cost	: 2,
			ip		: '192.168.43.116',
			port	: 9080,
			status	: false,
			hello	: {
				sent: false,
				recieved: false
			},
			welcome	: {
				sent: false,
				recieved: false
			}
		},
	],
	pretty: false,
	KeepAliveTimeout: 18
}
