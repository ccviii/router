'use strict';
/**
 *	Client
 */


/**
 * Dependencies
 */
const mySocket = require('./mySocket');
const _nodes = require('./conf').nodes

let node, client, options;

for (var i = 0; i < _nodes.length; i++) {
	node = _nodes[i];
	options = {
		port: node.port,
		host: node.ip
	}	
	_nodes[i].client = new mySocket(options, i, node);
};
