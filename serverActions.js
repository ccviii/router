'use strict'

/**
 * Dependencies
 */
const _ 	= require('lodash');
const debug = require('debug')('router:server');
const _conf = require('./conf');
const _util = require('./util')
const Client = require('./mySocket')
const Node = require('./Node')
const DV = require('./DV')
const purdy = require('purdy');
const mySocket = require('./mySocket')
global['_nodes'] = require('./conf').nodes
global['_SELF']  = {name: _conf.name, dv: {}}

global['_sendDV'] = force => {
	let len = Object.keys(_nodes).length;
	let forward = {};
	let firstDVtoSEND = false;
	let seMandoElDV = true;

	firstDVtoSEND = force;

	_util.log(forward)
	if(JSON.stringify(_SELF.dv)=='{}'){
		firstDVtoSEND = true;
		_util.log("caso 1")
		// caso 1
		for (var i = 0; i < Object.keys(_nodes).length; i++) {
			var key = Object.keys(_nodes)[i];
			forward[key] = {cost: _nodes[key].cost, via: key};
		};
	} else {
		_util.log("caso 2")
		// caso 2
		let keys = [Object.keys(_nodes)];
		_util.log(_nodes, true)

		for (var i = 0; i < Object.keys(_nodes).length; i++) {
			var key = Object.keys(_nodes)[i];
			if(_nodes[key].dv!=undefined){
				keys.push(Object.keys(_nodes[key].dv));
			}
		};

		keys = _.chain(keys).flatten()
			.map(key => key.toUpperCase()).uniq()
			.filter(key => key != _SELF.name).value()

		// keys ready!
		_util.log(keys)
		for (var i = 0; i < keys.length; i++) {
			var key = keys[i];
			// directo
			var min = (_nodes[key]!=undefined && _nodes[key].status)? {cost:_nodes[key].cost, via: key} : {cost: 99, via: '!'}
			for (var j = 0; j < Object.keys(_nodes).length; j++) {
				var k = Object.keys(_nodes)[j];
				if(_nodes[k].dv!=undefined){
					if(_nodes[k].dv[key.toLowerCase()]!=undefined){
						let cost = parseInt(_nodes[k].dv[key.toLowerCase()]) 
								+ parseInt(_nodes[k].cost)
						if(!_nodes[k].status)
							cost = 99 + parseInt(_nodes[k].dv[key.toLowerCase()]);
						if(cost < min.cost){
							min = {
								cost: cost,
								via: k
							}
						}
					} else {
						// console.log(">>", key)
					}
				}

			};
			// console.log(key, min)
			forward[key] = min
		};

	}

	// patch
	if(_SELF.dv.id) delete _SELF.dv.id

	// save
	// console.log("diff", _SELF,"2", forward)
	if(JSON.stringify(_SELF.dv) != JSON.stringify(forward) || firstDVtoSEND){
		_util.log("CAMBIO")
		_SELF.dv = forward;
	
		// guardar nuestro distance vector
		DV.save(forward);

		len = Object.keys(forward).length
		
		forward = _.reduce(Object.keys(forward), (accum, key)=> {
			accum += '\n'+key+':'+forward[key].cost
			return accum;
		}, "")

		
		// console.log(">>>", Object.keys(_nodes))
		// _util.log("FROM:"+_conf.name+"\nTYPE:DV\nLEN:"+len+forward+'\n\r\r')
		for (var i = 0; i < Object.keys(_nodes).length; i++) {
			let name = Object.keys(_nodes)[i];
			if(_nodes[name].status) {
				_util.log("SEND DV")
				_nodes[name].client.send("FROM:"+_conf.name+"\nTYPE:DV\nLEN:"+len+""+forward)
			}
		};
		// broadcast
		
		
	} else {
		_util.log("NO CAMBIO")
		seMandoElDV = false;
	}
	_util.log(_SELF)



	return seMandoElDV;
}

/**
 * Server Actions
 */
const actions = {
	onEnd: () => {
		debug('> client disconnected')
	},
	onData: (server, data) => {
		let res = _util.parseInfo(data.toString());

		debug(`> from: ${res.name} type: ${res.type}`)

		if(_nodes[res.name]==undefined) return debug(`> no es vecino [${res.name}]`)

		switch(res.type.toLowerCase()){
			case 'hello':
				_nodes[res.name].hello.recieved = true;
				_nodes[res.name].welcome.sent = true;

				debug(`> to: ${res.name} type: welcome`)

				if(!_nodes[res.name].hello.sent) {// si no ha enviado el hello y ahorita llego quiere volverse a conectar
					_nodes[res.name].client.reconnect();
					_sendDV(true);
				} else {
					// _sendDV();
					_nodes[res.name].client.send(`FROM:${_conf.name}\nTYPE:WELCOME`)
				}
				break
			case 'welcome':
				_nodes[res.name].welcome.recieved = true;
				Node.update(_nodes[res.name]);
				debug(`> from: ${res.name} type: welcome`)
				break
			case 'dv':
				debug(res.dv)
				debug("update dv for "+res.name)
				_nodes[res.name].dv = res.dv;
				// _sendDV();
				break
			default:
				debug('> ack')
				// server.write(`FROM:${_conf.name}\nTYPE:ACK\n\r\r`)
		}
	},
	onError: e => {
		debug(e)
	}
}


// RUN CLIENT
let node, client, options;

Node.find().then(data => {
	if(data.length==0){
		_util.log(_nodes)
		Node.create(_nodes).then(function(response){
			_util.log(response)
			for (var i = 0; i < _nodes.length; i++) {
				node = _nodes[i];

				options = {
					port: node.port,
					host: node.ip
				}	

				_nodes[i].id = response.generated_keys[i]
				_nodes[i].client = new mySocket(options, node.id, node);
			};
			_nodes = _util.array2Object(_nodes);

			_util.log({Message: "No hay datos"})
		})
	} else {
		_util.log("Ya hay datos en la DB")
		_nodes = data;

		for (var i = 0; i < _nodes.length; i++) {
			node = _nodes[i];

			options = {
				port: node.port,
				host: node.ip
			}
			
			_nodes[i].client = new mySocket(options, node.id, node);
		};
		_nodes = _util.array2Object(_nodes);
		_util.log(_nodes, true)
	}
});


setTimeout(() => {
	_util.log(_nodes, true);
	_util.log(_SELF);
}, 10 * 1000)

// BIND
module.exports = server => {
	debug('> client connected')
	server.on('end', actions.onEnd)
	server.on('data', data => actions.onData(server, data))
	server.on('error', e => actions.onError(e))
}
